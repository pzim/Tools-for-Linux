# Tools-for-Linux



Here are many tools to configure Linux :

- [VPNautoconnect](VPNautoconnect) : 

[Download :inbox_tray:](https://github.com/pzim-devdata/Tools-for-Linux/releases/download/v1.0.0/VPNautoconnect.zip)

A script to automatically connect to the VPN at startup (if you lauch this script at startup) and reconnect if connection is lost.

- [Volumouse](Volumouse) :

[Download :inbox_tray:](https://github.com/pzim-devdata/Tools-for-Linux/releases/download/v1.0.0/Volumouse.zip)

A Python 3 program that allows you to change the volume with the mouse wheel by using it in the four corners of the screen

- [Startminimized](Startminimized) :

[Download :inbox_tray:](https://github.com/pzim-devdata/Tools-for-Linux/releases/download/v1.0.0/Startminimized.zip)

A Python 3 program that allows you to start a program minimized. For exemple Thunderbird. To execute it tap : 
```ps
python3 "/PATH/TO/THE/PROGRAM/Startminimized.py" thunderbird
```
